<?php

namespace Drupal\lander\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Lander routes.
 */
class LanderController extends ControllerBase {

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Update a lander field.
   */
  public function update(Request $request, $entityType, $entityId, $entityFieldName, $delta) {
    $updateFields = json_decode($request->getContent(), TRUE);
    $entity = $this->entityTypeManager->getStorage($entityType)->load($entityId);
    if ($entity->hasField($entityFieldName)) {
      $values = $entity->get($entityFieldName)->getValue();
      $template = $values[$delta]['template'];
      $value = unserialize($values[$delta]['data']);
      foreach ($updateFields as $updateFieldName => $updateValue) {
        if (isset($value[$updateFieldName])) {
          $value[$updateFieldName] = $updateValue;
        }
      }
      $values[$delta] = [
        'template' => $template,
        'data' => serialize($value),
      ];
      $entity->set($entityFieldName, $values);
      $entity->save();
    }
    return new JsonResponse(json_encode($values[$delta]));
  }

}
