<?php

namespace Drupal\lander\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'lander' field widget.
 *
 * @FieldWidget(
 *   id = "lander",
 *   label = @Translation("Lander"),
 *   field_types = {"lander"},
 * )
 */
class LanderWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $type = \Drupal::service('plugin.manager.lander');
    $pluginDefinitions = $type->getDefinitions();
    $options = [];
    foreach ($pluginDefinitions as $pluginDefinition) {
      $options[$pluginDefinition['id']] = $pluginDefinition['label'];
    }

    $element['template'] = $element + [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => isset($items[$delta]->template) ? $items[$delta]->template : NULL,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(
    array $values,
    array $form,
    FormStateInterface $form_state
  ) {
    // The Lander widget does not provide an element to edit the data;
    // the editing feature is implemented in the lander field formatter
    // so we need to make sure that the field value is preserved, and not
    // overwritten with a null value when editors are saving an entity
    // form using the Lander widget.
    $entity = $form_state->getformObject()->getEntity();
    if ($form_state->getFormObject()->getFormId() != 'field_config_edit_form' && !$entity->isNew()) {
      $entityValues = $entity->{$this->fieldDefinition->getName()}->getValue();
      foreach ($values as $delta => &$value) {
        $value['data'] = $entityValues[$delta]['data'];
      }
    }
    return $values;
  }

}
