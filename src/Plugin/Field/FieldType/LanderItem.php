<?php

namespace Drupal\lander\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'lander' field type.
 *
 * @FieldType(
 *   id = "lander",
 *   label = @Translation("Lander"),
 *   category = @Translation("General"),
 *   default_widget = "lander",
 *   default_formatter = "lander"
 * )
 *
 * @DCG
 * If you are implementing a single value field type you may want to inherit
 * this class form some of the field type classes provided by Drupal core.
 * Check out /core/lib/Drupal/Core/Field/Plugin/Field/FieldType directory for a
 * list of available field type implementations.
 */
class LanderItem extends FieldItemBase {


  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('template')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    // @DCG
    // See /core/lib/Drupal/Core/TypedData/Plugin/DataType directory for
    // available data types.
    $properties['data'] = DataDefinition::create('string')
      ->setLabel(t('Text value'));

    $properties['template'] = DataDefinition::create('string')
      ->setLabel(t('Text value'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      'template' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Column description.',
        'length' => 255,
      ],
      'data' => [
        'type' => 'blob',
        'not null' => FALSE,
        'description' => 'Column description.',
        'size' => 'normal',
      ],
    ];

    $schema = [
      'columns' => $columns,
      // @DCG Add indexes here if necessary.
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();
    $data = $this->data;
    if (empty($data)) {
      $type = \Drupal::service('plugin.manager.lander');
      $instance = $type->createInstance($this->template);
      $variables = [];
      foreach ($instance->getFields() as $fieldName => $fieldValue) {
        $variables[$fieldName] = $fieldValue;
      }
      $data = serialize($variables);
      $this->data = $data;
    }
  }

}
