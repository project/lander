<?php

namespace Drupal\lander\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'Lander' formatter.
 *
 * @FieldFormatter(
 *   id = "lander",
 *   label = @Translation("Lander"),
 *   field_types = {
 *     "lander"
 *   }
 * )
 */
class LanderFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    if (!isset($element['#attached']['drupalSettings']['lander'])) {
      $element['#attached']['drupalSettings']['lander'] = [];
      $element['#attached']['drupalSettings']['lander']['fields'] = [];
    }
    $landerJsSettings = &$element['#attached']['drupalSettings']['lander'];
    $landerJsSettings['fields'][$this->fieldDefinition->getName()];
    foreach ($items as $delta => $item) {
      $landerJsSettings['fields'][$this->fieldDefinition->getName()][] = $delta;
      $entity = $item->getEntity();
      $fields = unserialize($item->get('data')->getValue());
      $fieldsVariables = [];
      $element['#attached']['library'][] = 'lander/lander';
      foreach ($fields as $field => $value) {
        $fieldsVariables[$field] = $value;
      }
      $element[$delta] = [
        '#theme' => 'lander_' . $item->get('template')->getValue(),
        '#entity_id' => $entity->id(),
        '#entity_type' => $entity->getEntityTypeId(),
        '#entity_field_name' => $this->fieldDefinition->getName(),
        '#delta' => $delta,
        '#fields' => $fieldsVariables,
      ];
    }
    return $element;
  }

}
