<?php

namespace Drupal\lander;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for lander plugins.
 */
abstract class LanderPluginBase extends PluginBase implements LanderInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
