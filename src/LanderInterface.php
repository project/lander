<?php

namespace Drupal\lander;

/**
 * Interface for lander plugins.
 */
interface LanderInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the fields defined by the lander plugin.
   *
   * @return array
   *   An array of field names.
   */
  public function getFields();

}
