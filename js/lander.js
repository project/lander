(function ($) {

  function Lander(landerEl, landerId, entityType, entityId, entityFieldName, delta) {
    this.basePath = '/lander/update/';
    this.dataStore = [];
    this.landerEl = landerEl;
    this.landerId = landerId;
    this.entityType = entityType;
    this.entityId = entityId;
    this.entityFieldName = entityFieldName;
    this.delta = delta;
    const instance = this;
    this.editBtn = this.createBtn('Edit', 'edit', this.edit.bind(this), 'Edit these fields');
    this.landerEl.appendChild(this.editBtn);

    this.cancelBtn = this.createBtn('Cancel', 'cancel', this.cancel.bind(this), 'Cancel operation');
    this.cancelBtn.style.display = 'none';
    this.landerEl.appendChild(this.cancelBtn);

    this.saveBtn = this.createBtn('Save', 'save', this.save.bind(this), 'Save your changes');
    this.landerEl.appendChild(this.saveBtn);
    this.saveBtn.style.display = 'none';

    this.fieldSelector = `[data-lander-field][data-lander-id="${this.landerId}"`;
    this.landerSections = document.querySelectorAll(this.fieldSelector);

    this.storefieldData();
  }

  Lander.prototype.storefieldData = function() {
    this.dataStore = [];
    this.dataStore = Array.from(this.landerSections).map(function (fieldEl) {
      return {el: fieldEl, val: fieldEl.innerHTML };
    });
  };

  Lander.prototype.resetFieldData = function() {
    const _this = this;
    this.landerSections.forEach(function (fieldEl) {
      const store = _this.dataStore.find(function (item) { return item.el === fieldEl });
      if (store) {
        fieldEl.innerHTML = store.val;
      }
    });
  };

  Lander.prototype.toggleEditable = function(editable) {
    this.landerSections.forEach(function (fieldEl) {
      fieldEl.setAttribute('contenteditable', editable.toString());
      if (editable) {
        fieldEl.classList.add('active');
        setTimeout(function () { fieldEl.classList.remove('active')}, 400);
      }
    });
  };

  Lander.prototype.cancel = function () {
    this.resetFieldData();
    this.toggleEditable(false);
    this.editBtn.style.display = 'block';
    this.saveBtn.style.display = 'none';
    this.cancelBtn.style.display = 'none';
  };

  Lander.prototype.save = function () {
    const _this = this;
    this.toggleEditable(false);
    const postUrl = `${this.basePath}${this.entityType}/${this.entityId}/${this.entityFieldName}/${this.delta}?XDEBUG_SESSION_START`;
    const landerFields = document.querySelectorAll(this.fieldSelector);
    let updatePayload = {};
    landerFields.forEach(function (landerField) {
      updatePayload[landerField.getAttribute('data-lander-field-name')] = landerField.innerHTML.replace(/^\s+|\s+$/g, '');
    });
    $.ajax({
      type: "POST",
      url: postUrl,
      data: JSON.stringify(updatePayload),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    }).done(function () {
      _this.storefieldData();
    })
    .fail(function () {
      _this.resetFieldData();
    });
    this.editBtn.style.display = 'block';
    this.saveBtn.style.display = 'none';
    this.cancelBtn.style.display = 'none';
  };

  Lander.prototype.edit = function () {
    this.toggleEditable(true);
    this.editBtn.style.display = 'none';
    this.saveBtn.style.display = 'block';
    this.cancelBtn.style.display = 'block';
  };

  Lander.prototype.createBtn = function (btnText, type, clickListener, ariaText) {
    const btn = document.createElement("button");
    btn.classList.add('lander-btn', 'lander-btn--' + type);
    btn.addEventListener('click', clickListener);
    btn.setAttribute('aria-label', ariaText);
    const txt = document.createTextNode(btnText);
    btn.appendChild(txt);
    return btn;
  };

  window.landers = [];
  const landerSections = document.querySelectorAll('[data-lander]');
  landerSections.forEach(function (landerSec) {
    const landerId = landerSec.getAttribute('data-lander-id');
    const entityType = landerSec.getAttribute('data-entity-type');
    const entityId = landerSec.getAttribute('data-entity-id');
    const entityField = landerSec.getAttribute('data-entity-field-name');
    const delta = landerSec.getAttribute('data-delta');
    window.landers.push(new Lander(landerSec, landerId, entityType, entityId, entityField, delta));
  });

})(jQuery);
