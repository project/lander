<?php

namespace Drupal\lander_example\Plugin\Lander;

use Drupal\lander\LanderPluginBase;

/**
 * Plugin implementation of the lander.
 *
 * @Lander(
 *   id = "lander_example",
 *   label = @Translation("Lander example"),
 *   description = @Translation("An example page that uses the Lander API.")
 * )
 */
class LanderExample extends LanderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    return [
      'hero_heading' => 'Lorem ipsum dolor sit amet',
      'hero_text' => 'Morbi odio nisl, volutpat non elementum et, dapibus in turpis. Phasellus eleifend risus ut lorem vulputate, at tincidunt urna consectetur.',
    ];
  }

}
